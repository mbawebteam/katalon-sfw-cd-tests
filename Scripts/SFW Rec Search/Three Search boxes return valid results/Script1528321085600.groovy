import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.SFW_URL)

WebUI.setText(findTestObject('Object Repository/SFW Rec Search/input_typeahead tt-input'), 'barnacle')

WebUI.sendKeys(findTestObject('Object Repository/SFW Rec Search/input_typeahead tt-input'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Overview Page/Page_Barnacle/h1_Barnacle'))

WebUI.doubleClick(findTestObject('Overview Page/Page_Barnacle/input_typeahead ng-pristine ng'))

WebUI.setText(findTestObject('Overview Page/Page_Barnacle/input_typeahead ng-pristine ng'), 'salmon')

WebUI.sendKeys(findTestObject('Overview Page/Page_Barnacle/input_typeahead ng-pristine ng'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('Overview Page/Page_Salmon/h1_Salmon'), 10)

WebUI.verifyElementPresent(findTestObject('Overview Page/Page_Salmon/h1_Salmon'), 3)

WebUI.click(findTestObject('Overview Page/Page_Salmon/a_View Salmon Recommendations'))

WebUI.verifyElementPresent(findTestObject('Overview Page/Salmon Recommendations/h1_Salmon Recommendations'), 3)

WebUI.waitForPageLoad(3)

WebUI.verifyElementPresent(findTestObject('SFW Rec Search/Search Again Label'), 2)

WebUI.verifyElementVisible(findTestObject('SFW Rec Search/Rec Search Icon'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Overview Page/Salmon Recommendations/input_typeahead ng-valid tt-in'), 3)

WebUI.click(findTestObject('Overview Page/Salmon Recommendations/input_typeahead ng-valid tt-in'))

WebUI.clearText(findTestObject('Overview Page/Salmon Recommendations/input_typeahead ng-valid tt-in'))

WebUI.setText(findTestObject('Overview Page/Salmon Recommendations/input_typeahead ng-valid tt-in'), 'abalone')

WebUI.sendKeys(findTestObject('Overview Page/Salmon Recommendations/input_typeahead ng-valid tt-in'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('Overview Page/Page_Abalone/h1_Abalone'), 10)

WebUI.verifyElementPresent(findTestObject('Overview Page/Page_Abalone/h1_Abalone'), 3)

WebUI.closeBrowser()

