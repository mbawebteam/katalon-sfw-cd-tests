import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.SFW_URL)

WebUI.setText(findTestObject('Object Repository/SFW Rec Search/input_typeahead tt-input'), 'common')

WebUI.sendKeys(findTestObject('Object Repository/SFW Rec Search/input_typeahead tt-input'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(10)

//user is at the multi-group page

WebUI.waitForElementPresent(findTestObject('Multi Group Page/Filter Type is populated on the SFW Recommendations Page/img_img'), 5)

WebUI.click(findTestObject('Multi Group Page/Filter Type is populated on the SFW Recommendations Page/img_img'))

//user is at the Recommendations page

WebUI.click(findTestObject('Overview Page/Cockle Recommendations/a_View Cockle Recommendations'))

//user is the the Cockle Recommendations page and "Common" shows in the typ filter.

WebUI.waitForElementPresent(findTestObject('Recommendations Page/Cockle Recommendations Page/h1_Cockle Recommendations'), 3)

WebUI.verifyElementPresent(findTestObject('Recommendations Page/Cockle Recommendations Page/h1_Cockle Recommendations'), 3)

WebUI.verifyElementPresent(findTestObject('Recommendations Page/Cockle Recommendations Page/p_Common'), 3)

WebUI.closeBrowser()

