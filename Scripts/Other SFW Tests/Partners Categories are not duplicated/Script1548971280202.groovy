import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.By

WebUI.openBrowser(GlobalVariable.SFW_URL + '/businesses-and-organizations/partners')

WebDriver driver = DriverFactory.getWebDriver()

elementCount = driver.findElements(By.xpath('/html/body/main/div/div/div/div[1]//abbr[@title ="Business Partner"]')).size()
println elementCount
assert 1 == elementCount

elementCount = driver.findElements(By.xpath('/html/body/main/div/div/div/div[1]//abbr[@title ="Restaurant Partner"]')).size()
println elementCount
assert 1 == elementCount

elementCount = driver.findElements(By.xpath('/html/body/main/div/div/div/div[1]//abbr[@title ="Industry Collaborator"]')).size()
println elementCount
assert 1 == elementCount

elementCount = driver.findElements(By.xpath('/html/body/main/div/div/div/div[1]//abbr[@title ="Conservation Partner"]')).size()
println elementCount
assert 1 == elementCount