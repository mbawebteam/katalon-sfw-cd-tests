import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.SFW_URL + '/businesses-and-organizations/partners')

WebUI.maximizeWindow()

WebUI.waitForPageLoad(4)

WebUI.verifyElementPresent(findTestObject('Other SFW Tests/Red Lobster BP Search/input'), 0)

WebUI.verifyElementClickable(findTestObject('Other SFW Tests/Red Lobster BP Search/input'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Other SFW Tests/Red Lobster BP Search/input'))

WebUI.clearText(findTestObject('Other SFW Tests/Red Lobster BP Search/input'))

WebUI.setText(findTestObject('Other SFW Tests/Red Lobster BP Search/input'), 'Red lobster')

WebUI.selectOptionByValue(findTestObject('Other SFW Tests/Red Lobster BP Search/Show 100 Entries'), '100', true)

WebUI.verifyElementPresent(findTestObject('Other SFW Tests/Red Lobster BP Search/Red Lobster (10th St)'), 2)

WebUI.verifyElementPresent(findTestObject('Other SFW Tests/Red Lobster BP Search/abbr_RP'), 2)

WebUI.verifyElementPresent(findTestObject('Other SFW Tests/Red Lobster BP Search/City_Huntington'), 2)

WebUI.verifyElementPresent(findTestObject('Other SFW Tests/Red Lobster BP Search/state_West Virginia'), 2)

WebUI.verifyElementPresent(findTestObject('Other SFW Tests/Red Lobster BP Search/Red Lobster (13th Ave S)'), 2)

WebUI.verifyElementPresent(findTestObject('Other SFW Tests/Red Lobster BP Search/Red Lobster (Ala Moana Bl'), 2)

WebUI.closeBrowser()

