<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Red Lobster (10th St)</name>
   <tag></tag>
   <elementGuidId>32c65d18-127c-4edb-8ec2-1aad7290c7f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;business-partners-table&quot;]/tbody/tr[1]/td[1]/a[count(. | //*[(text() = 'Red Lobster (10th St) ' or . = 'Red Lobster (10th St) ')]) = count(//*[(text() = 'Red Lobster (10th St) ' or . = 'Red Lobster (10th St) ')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Red Lobster (10th St) </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;business-partners-table&quot;]/tbody/tr[1]/td[1]/a</value>
   </webElementProperties>
</WebElementEntity>
