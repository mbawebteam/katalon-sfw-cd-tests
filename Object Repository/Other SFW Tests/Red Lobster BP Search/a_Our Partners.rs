<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Our Partners</name>
   <tag></tag>
   <elementGuidId>98a29621-3617-4baf-ab1a-ed036add8900</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/businesses-and-organizations/partners</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Our Partners</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js history video&quot;]/body[@class=&quot;sfw sfw-home menu-active&quot;]/header[@class=&quot;site-header subnav-open&quot;]/div[@class=&quot;mask&quot;]/div[@class=&quot;container&quot;]/nav[@class=&quot;site-nav&quot;]/ul[@class=&quot;global-nav&quot;]/li[@class=&quot;topnav active open&quot;]/div[@class=&quot;subnav-bar&quot;]/ul[@class=&quot;subnav&quot;]/li[2]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
