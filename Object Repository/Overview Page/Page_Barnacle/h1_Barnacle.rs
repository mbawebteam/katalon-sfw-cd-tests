<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Barnacle</name>
   <tag></tag>
   <elementGuidId>29faae77-5a12-4b7b-b501-6537d616c914</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fish-title centered nomargintop ng-binding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Barnacle</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js history video&quot;]/body[@class=&quot;sfw sticky-search&quot;]/main[@class=&quot;site-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;blue-box-rec-search rounded-corners&quot;]/div[@class=&quot;view-container rounded-corners white-box border&quot;]/div[1]/div[@class=&quot;views ng-scope&quot;]/div[@class=&quot;view-overview row fadeIn ng-scope&quot;]/div[@class=&quot;row centered&quot;]/h1[@class=&quot;fish-title centered nomargintop ng-binding&quot;]</value>
   </webElementProperties>
</WebElementEntity>
