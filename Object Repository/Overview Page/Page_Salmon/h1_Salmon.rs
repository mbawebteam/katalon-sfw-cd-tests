<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Salmon</name>
   <tag></tag>
   <elementGuidId>7d53ac09-2580-48b4-a328-a0f622bdc8a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h1[@class = 'fish-title centered nomargintop ng-binding' and (text() = 'Salmon' or . = 'Salmon')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fish-title centered nomargintop ng-binding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Salmon</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/main/div/div/div/div/div/div[2]/div/div/div[2]/div[1]/h1</value>
   </webElementProperties>
</WebElementEntity>
