<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Shrimp Recommendations</name>
   <tag></tag>
   <elementGuidId>f83eeb8e-6569-4574-88cd-28b04c873614</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page-title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Shrimp Recommendations</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js history video&quot;]/body[@class=&quot;sfw&quot;]/main[@class=&quot;site-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;page&quot;]/header[@class=&quot;page-header&quot;]/h1[@class=&quot;page-title&quot;]</value>
   </webElementProperties>
</WebElementEntity>
