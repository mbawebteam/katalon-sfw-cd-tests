<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_img_1</name>
   <tag></tag>
   <elementGuidId>aa98a869-f89c-45c9-a6eb-1b792a086f98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//img[@class = 'img' and @alt = 'Whitefish, Lake, Monterey Bay Aquarium Seafood Watch' and @title = '© New York State Dept. of Environmental Conservation']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Whitefish, Lake, Monterey Bay Aquarium Seafood Watch</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>© New York State Dept. of Environmental Conservation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js history video&quot;]/body[@class=&quot;sfw sticky-search&quot;]/main[@class=&quot;site-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;blue-box-rec-search rounded-corners&quot;]/div[@class=&quot;view-container rounded-corners white-box border&quot;]/div[1]/div[@class=&quot;views ng-scope&quot;]/div[@class=&quot;view-groups row fadeIn ng-scope&quot;]/div[@class=&quot;container az-list&quot;]/div[@class=&quot;image-grid&quot;]/a[@class=&quot;image-grid-item ng-scope&quot;]/figure[@class=&quot;image-grid-item-inner&quot;]/img[@class=&quot;img&quot;]</value>
   </webElementProperties>
</WebElementEntity>
