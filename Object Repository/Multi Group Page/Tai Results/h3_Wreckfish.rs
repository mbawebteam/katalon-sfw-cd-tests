<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Wreckfish</name>
   <tag></tag>
   <elementGuidId>41748ec7-c7fe-4750-a485-8fed47573643</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>image-grid-item-title ng-binding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Wreckfish</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js history video&quot;]/body[@class=&quot;sfw sticky-search&quot;]/main[@class=&quot;site-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;blue-box-rec-search rounded-corners&quot;]/div[@class=&quot;view-container rounded-corners white-box border&quot;]/div[1]/div[@class=&quot;views ng-scope&quot;]/div[@class=&quot;view-groups row fadeIn ng-scope&quot;]/div[@class=&quot;container az-list&quot;]/div[@class=&quot;image-grid&quot;]/a[@class=&quot;image-grid-item ng-scope&quot;]/figure[@class=&quot;image-grid-item-inner&quot;]/figcaption[@class=&quot;image-grid-item-details&quot;]/h3[@class=&quot;image-grid-item-title ng-binding&quot;]</value>
   </webElementProperties>
</WebElementEntity>
