<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Last Image</name>
   <tag></tag>
   <elementGuidId>bb48a7f0-1b17-41f5-bee5-08d390aa745d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//img[@class = 'img' and @alt = 'Tuna, Yellowfin, Monterey Bay Aquarium Seafood Watch' and @src = 'https://qa-cms-cd.seafoodwatch.org:443/-/m/sfw/images/recommendations/fish/tuna/yellowfin_tuna.png?w=150&amp;h=100&amp;bc=ffffff ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-src</name>
      <type>Main</type>
      <value>https://qa-cms-cd.seafoodwatch.org:443/-/m/sfw/images/recommendations/fish/tuna/yellowfin_tuna.png?w=150&amp;h=100&amp;bc=ffffff </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Tuna, Yellowfin, Monterey Bay Aquarium Seafood Watch</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>© New York State Dept. of Environmental Conservation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://qa-cms-cd.seafoodwatch.org:443/-/m/sfw/images/recommendations/fish/tuna/yellowfin_tuna.png?w=150&amp;h=100&amp;bc=ffffff </value>
   </webElementProperties>
</WebElementEntity>
