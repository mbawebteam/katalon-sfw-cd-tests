<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Tuna Albacore</name>
   <tag></tag>
   <elementGuidId>a8ceae9f-f74c-4404-a9e5-7c46a78f274f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tuna, Albacore</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js history video&quot;]/body[@class=&quot;sfw sfw-home&quot;]/main[@class=&quot;site-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;white-box read-more-wrap&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;six columns omega&quot;]/div[@class=&quot;search-wrap&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;search-bar columns eight&quot;]/form[1]/span[@class=&quot;twitter-typeahead&quot;]/span[@class=&quot;tt-dropdown-menu&quot;]/div[@class=&quot;tt-dataset-suggestions&quot;]/span[@class=&quot;tt-suggestions&quot;]/div[@class=&quot;tt-suggestion tt-cursor&quot;]/span[@class=&quot;content&quot;]/span[@class=&quot;title&quot;]</value>
   </webElementProperties>
</WebElementEntity>
