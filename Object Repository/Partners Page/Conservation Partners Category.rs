<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Conservation Partners Category</name>
   <tag></tag>
   <elementGuidId>5aa220a7-7da6-4205-b51f-33f02618d91b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>body > main > div > div > div > div.row > div:nth-child(1) > section > h3:nth-child(4)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>body > main > div > div > div > div.row > div:nth-child(1) > section > h3:nth-child(4)</value>
   </webElementProperties>
</WebElementEntity>
