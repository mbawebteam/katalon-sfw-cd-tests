<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Fish Title</name>
   <tag></tag>
   <elementGuidId>f6f0b79b-a8b7-47d4-b15d-7dd51f28d757</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h1[@class = 'fish-title nomarginbottom ng-binding']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fish-title nomarginbottom ng-binding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
   </webElementProperties>
</WebElementEntity>
