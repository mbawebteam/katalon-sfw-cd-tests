<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Seafood Watch Tab</name>
   <tag></tag>
   <elementGuidId>3768f5b3-3544-407d-96b3-4d2e397d6511</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursor-pointer active noEcoCerts</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>recommendationType = 'standard'</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>{ 'active': recommendationType === 'standard', 'noEcoCerts' : model.recommendations.ecoCertified.length === 0 }</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-show</name>
      <type>Main</type>
      <value>model.recommendations.standard.length > 0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Seafood Watch</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js history video&quot;]/body[@class=&quot;sfw&quot;]/main[@class=&quot;site-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;blue-box-rec-search rounded-corners&quot;]/div[@class=&quot;view-container rounded-corners white-box border&quot;]/div[1]/div[@class=&quot;views ng-scope&quot;]/div[@class=&quot;view-results row fadeIn ng-scope&quot;]/div[@class=&quot;twelve columns&quot;]/div[@class=&quot;result row&quot;]/div[@class=&quot;results&quot;]/div[@class=&quot;results-nav&quot;]/ul[1]/li[@class=&quot;cursor-pointer active noEcoCerts&quot;]</value>
   </webElementProperties>
</WebElementEntity>
