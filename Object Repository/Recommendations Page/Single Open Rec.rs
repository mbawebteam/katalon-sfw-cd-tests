<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Single Open Rec</name>
   <tag></tag>
   <elementGuidId>74e14dce-58ff-4996-ab5a-bc2a32d41380</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-show = 'rec.scoringInfo.hideOverallScore === false' and (contains(text(), 'Overall Score') or contains(., 'Overall Score'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-show</name>
      <type>Main</type>
      <value>rec.scoringInfo.hideOverallScore === false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Overall Score</value>
   </webElementProperties>
</WebElementEntity>
