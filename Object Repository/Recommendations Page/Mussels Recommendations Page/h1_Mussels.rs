<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Mussels</name>
   <tag></tag>
   <elementGuidId>9f0f4df6-f749-409b-988f-77d711c18f16</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h1[@class = 'fish-title nomarginbottom ng-binding' and (text() = 'Mussels' or . = 'Mussels')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fish-title nomarginbottom ng-binding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Mussels</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js history video&quot;]/body[@class=&quot;sfw sticky-search&quot;]/main[@class=&quot;site-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;blue-box-rec-search rounded-corners&quot;]/div[@class=&quot;view-container rounded-corners white-box border&quot;]/div[1]/div[@class=&quot;views ng-scope&quot;]/div[@class=&quot;view-results row fadeIn ng-scope&quot;]/div[@class=&quot;twelve columns&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;six-columns omega&quot;]/div[@class=&quot;row&quot;]/h1[@class=&quot;fish-title nomarginbottom ng-binding&quot;]</value>
   </webElementProperties>
</WebElementEntity>
