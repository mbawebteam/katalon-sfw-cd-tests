<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Other SFW Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ee51d73d-5e54-4e8f-8304-40a964813798</testSuiteGuid>
   <testCaseLink>
      <guid>4567f868-4d2c-43f8-8449-8c56163ebe54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Other SFW Tests/Red Lobster BP Search by State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c697f074-30df-42cc-bcc3-026fac4aa902</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Other SFW Tests/Red Lobster BP Search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47bc6326-f9ab-48b8-8ccb-5b74e9d5665b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Other SFW Tests/Partners Categories are not duplicated</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
