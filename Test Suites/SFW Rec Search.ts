<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SFW Rec Search</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-03T15:43:11</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>974fee32-cc45-417c-b75a-fce22fc1b484</testSuiteGuid>
   <testCaseLink>
      <guid>830ec1a5-3324-479d-951c-3b837659c601</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Aquaculture Returns multi groups</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96c61e74-fef1-4ba7-96f9-b7723a67a935</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Chilean Returns Mussels, Chilean</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>219b5098-cf79-44e1-a2d7-fa7d7b07d97a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Farmed Returns multi groups</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64f5cbb9-d72e-40ad-831e-7e90a8844392</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Filter Type is populated on SFW Recommendations Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>937e61fe-4d88-4807-9afe-061331b3d127</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Imported Returns Multi Group Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7223bff8-cc62-4e1d-8184-0a5298db1e58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/No Matches with Chopper</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>468368e1-de08-4fb7-8b9f-66f492595da9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Market name Crabeater</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77556ba1-4f4e-4912-afea-d879efb42f82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Mult-group Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fca13667-4a56-4385-933e-a8c587b09c77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/No Matches Found</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>697ca800-cac4-412e-9820-adb270a3a39d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Search opens Eco-cert rec</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1e0a66e-94e6-4f61-9f64-9e1e295ccb9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Search opens SFW tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60e3e51c-d306-4258-a776-28bc6615a7a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Shrimp Group Drop Down</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3159810-20f3-4b96-9dc0-c190fa6b0014</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Three Search boxes return valid results</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74da3ea2-573e-400f-b448-f33ea3687429</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Tilapia Overview</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6ce4d67-f882-4509-a23f-e1f8edd7e693</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Tuna Albacore Drop Down</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35932859-4d10-403b-b019-4be48767b718</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Wakame returns Seaweed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b7ef308-ed1e-43f1-9fa6-3d9df5557449</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SFW Rec Search/Wreckfish Opens Single SFW Rec</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
